#!/usr/bin/env bash
#
# update WP-CLI
# since Scotch Box pro (php7), we have to reinstall WP-CLI
echo "== Update WP CLI (re-install) =="
sudo rm /usr/local/bin/wp
sudo curl -O https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar
sudo chmod +x wp-cli.phar
sudo mv wp-cli.phar /usr/local/bin/wp
echo "WP-CLI Update successful"
composer global require hirak/prestissimo
# echo "Lumberjack-installer Install"
# composer --global config process-timeout 0
# composer global require rareloop/lumberjack-bedrock-installer
# echo "Lumberjack-installer Install successful"

source /var/www/provision/variables.inc.sh
source /var/www/provision/utils.sh
source /var/www/provision/lumberjack.sh
source /var/www/provision/apache.sh