let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for your application, as well as bundling up your JS files.
 |
 */
// mix.autoload({
//     jquery: ['$', 'window.jQuery']
// });

mix
    .options({
        processCssUrls: false,
    })
    .setPublicPath('./dist/')
    .sass('assets/css/main.scss', 'dist/css/')
    .js('assets/js/main.js', 'dist/js/')
    .copyDirectory("assets/images", "dist/images/")
    .copyDirectory("assets/fonts", "dist/fonts/");



if (mix.inProduction()) {
    mix.version();
} else {
    mix.sourceMaps();
    mix.browserSync({
        proxy: 'local.test',
        files: ["*.html", "dist/**/*.*"]
    });
}
