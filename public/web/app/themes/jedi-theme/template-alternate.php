<?php
/**
 * Template Name: Landing page alternative 1
 * The template for displaying service pages.
 *.
 */

namespace App;

use App\Http\Controllers\Controller;
use Rareloop\Lumberjack\Http\Responses\TimberResponse;
use Timber\Timber;

class TemplateAlternateController extends Controller
{
    public function handle()
    {
        $context = Timber::get_context();
        $page = $context['posts'][0];

        $context['post'] = $page;
        $context['title'] = $page->title;
        $context['content'] = $page->content;
        $context['thumbnail'] = $page->thumbnail();
        $context['anchors'] = $page->meta('anchors');
        $context['intro'] = $page->meta('intro');
        $context['advantages'] = $page->meta('advantages');
        $context['reassurance'] = $page->meta('reassurance');
        $context['network'] = $page->meta('network');
        $context['testimonials'] = $page->meta('testimonials');
        $context['prices'] = $page->meta('prices');
        $context['team'] = $page->meta('team');
        $context['faq'] = $page->meta('faq');
        $context['video'] = $page->meta('video');
        $context['quotation'] = $page->meta('quotation');

        return new TimberResponse('templates/alternate-landingpage.twig', $context);
    }
}
