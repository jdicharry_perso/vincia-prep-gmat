<?php
use App\Http\Gutenberg;

return [
    'type_loading_fields' => 'JSON', // PHP|JSON
    'autoloading' => true,
    'fields' => get_stylesheet_directory() . '/acf-json-fields',
    'block_type' => [
        // 'Exemple' => [
        //     'name'            => 'exemple',
        //     'title'           => __('Exemple', 'jedi-theme'),
        //     'render_callback' => [Gutenberg::class, 'renderMod'],
        //     'category'        => 'mods',
        //     'icon'            => 'share-alt2',
        //     'supports'        => ['align' => false]
        // ],
    ],
    'options' => [
        'main' => [
            'page_title' => 'Theme settings',
            'menu_title' => 'Theme settings',
            'menu_slug' => 'theme-settings',
            'capability' => 'edit_posts',
            'redirect' => false
        ]
    ]
];
