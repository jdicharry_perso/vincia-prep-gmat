<?php

/**
 * The Template for the front-page
 */

namespace App;

use App\Http\Controllers\Controller;
use Rareloop\Lumberjack\Http\Responses\TimberResponse;
use Timber\Timber;

class FrontPageController extends Controller
{
    public function handle()
    {
        $context = Timber::get_context();
        $page = $context['posts'][0];

        $context['post'] = $page;
        $context['title'] = $page->title;
        $context['content'] = $page->content;
        $context['thumbnail'] = $page->thumbnail();
        $context['anchors'] = $page->meta('anchors');
        $context['intro'] = $page->meta('intro');
        $context['advantages'] = $page->meta('advantages');
        $context['reassurance'] = $page->meta('reassurance');
        $context['network'] = $page->meta('network');
        $context['testimonials'] = $page->meta('testimonials');
        $context['prices'] = $page->meta('prices');
        $context['team'] = $page->meta('team');
        $context['contact'] = $page->meta('contact');
        $context['video'] = $page->meta('video');
        $context['quotation'] = $page->meta('quotation');

        return new TimberResponse('templates/front-page.twig', $context);
    }
}
