<?php

namespace App\Providers;

use Rareloop\Lumberjack\Providers\ServiceProvider;

class WysiwygServiceProvider extends ServiceProvider
{
    /**
     * Register required items with the Application Container
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Perform any required boot operations
     *
     * @return void
     */
    public function boot()
    {
        add_filter('mce_buttons_2', [$this, 'mce_buttons_2']);
        add_filter('tiny_mce_before_init', [$this, 'mce_custom_styles']);
    }

    public function mce_buttons_2($buttons)
    {
        array_unshift($buttons, 'styleselect');
        return $buttons;
    }

    public function mce_custom_styles($init_array)
    {
        $style_formats = [
            [
                'title' => 'Button',
                'inline' => 'a',
                'classes' => 'button',
                'wrapper' => true,
            ],
        ];

        $init_array['style_formats'] = json_encode($style_formats);

        return $init_array;
    }
}
