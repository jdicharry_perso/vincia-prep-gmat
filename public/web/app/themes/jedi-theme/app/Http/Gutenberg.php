<?php

namespace App\Http;

use Rareloop\Lumberjack\Page;
use Timber\Timber;

class Gutenberg
{
    public static function renderMod($block)
    {
        // convert name ("acf/testimonial") into path friendly slug ("testimonial")
        $slug = str_replace('acf/', '', $block['name']);

        $context = Timber::get_context();

        // Store block values.
        $context['block'] = $block;

        // Store field values.
        $context['fields'] = get_fields();

        if (file_exists(get_theme_file_path("/views/flexibles/{$slug}.twig"))) {
            return Timber::render("flexibles/{$slug}.twig", $context);
        }
    }
}
