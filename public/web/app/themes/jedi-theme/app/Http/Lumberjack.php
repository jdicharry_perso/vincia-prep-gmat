<?php

namespace App\Http;

use Rareloop\Lumberjack\Http\Lumberjack as LumberjackCore;
use App\Menu\Menu;

class Lumberjack extends LumberjackCore
{
    public function addToContext($context)
    {
        $context['is_home'] = is_home();
        $context['is_front_page'] = is_front_page();
        $context['is_logged_in'] = is_user_logged_in();
        $context['options'] = get_fields('options');
        $context['menu'] = $this->getMainMenu();

        return $context;
    }

    private function getMainMenu()
    {
        return wp_nav_menu([
            'echo' => false,
            'theme_location' => 'main-nav',
            'container' => 'div',
            'menu_id' => 'primary-menu',
            'menu_class' => 'nav'
        ]);
    }
}
