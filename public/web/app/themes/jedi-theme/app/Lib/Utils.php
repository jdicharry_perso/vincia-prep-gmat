<?php

namespace App\Lib;

use App\Exceptions\PostTypeExistException;
use Rareloop\Lumberjack\Facades\Config;
use Rareloop\Lumberjack\Page;
use Rareloop\Lumberjack\Post;

class Utils {

    public static function getPostClassByType($type = null)
    {
        $registerPostTypes = Config::get('posttypes.register');
        $postTypes = [
            'page' => Page::class,
            'post' => Post::class
        ];

        foreach ($registerPostTypes as $postType) {
            $postTypes[$postType::getPostType()] = $postType;
        }

        if( $type ) {
            if(array_key_exists($type, $postTypes)) {
                return $postTypes[$type];
            } else {
                throw new PostTypeExistException("The post type '$type' doesn't exist.");
            }
        }

        return $postTypes;
    }

    public static function YbOembedToId($oembed)
    {
        if( preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $oembed, $match) ) {
            return $match[1];
        }

        return false;
    }
}
