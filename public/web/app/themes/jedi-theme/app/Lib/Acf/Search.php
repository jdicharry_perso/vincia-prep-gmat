<?php

namespace App\Lib\Acf;

class Search {

    public function __construct()
    {
        $this->load();
    }

    public function load()
    {
        add_filter('posts_join', [$this, 'search_join']);
        add_filter('posts_where', [$this, 'search_where']);
        add_filter('posts_distinct', [$this, 'search_distinct']);
    }

    // Make the search to index custom
    /**
     * Extend WordPress search to include custom fields
     * http://adambalee.com
     *
     * Join posts and postmeta tables
     * http://codex.wordpress.org/Plugin_API/Filter_Reference/posts_join
     */
    public function search_join($join)
    {
        global $wpdb;
        if (is_search()) {
            $join .= ' LEFT JOIN ' . $wpdb->postmeta . ' cfmeta ON ' . $wpdb->posts . '.ID = cfmeta.post_id ';
        }
        return $join;
    }

    /**
     * Modify the search query with posts_where
     * http://codex.wordpress.org/Plugin_API/Filter_Reference/posts_where
     */
    public function search_where($where)
    {
        global $pagenow, $wpdb;
        if (is_search()) {
            $where = preg_replace(
                "/\(\s*" . $wpdb->posts . ".post_title\s+LIKE\s*(\'[^\']+\')\s*\)/",
                "(" . $wpdb->posts . ".post_title LIKE $1) OR (cfmeta.meta_value LIKE $1)",
                $where
            );
        }
        return $where;
    }

    /**
     * Prevent duplicates
     * http://codex.wordpress.org/Plugin_API/Filter_Reference/posts_distinct
     */
    public function search_distinct($where)
    {
        global $wpdb;
        if (is_search()) {
        return "DISTINCT";
        }
        return $where;
    }
}
