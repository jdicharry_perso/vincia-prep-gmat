import $ from "jquery";
window.jQuery = $;
window.$ = $;

import "foundation-sites";
import "intersection-observer/intersection-observer.js";

$(".js--show").on("click", function() {
    $(".hidden").removeClass("hidden");
});

const lq = window.matchMedia("(max-width: 1320px)");
const mq = window.matchMedia("(max-width: 1024px)");
const sq = window.matchMedia("(max-width: 640px)");

// ********* Apparitions au scroll *********
if (sq.matches) {
    var threshold = 0.05;
} else if (mq.matches) {
    var threshold = 0.15;
} else {
    var threshold = 0.25;
}
const options = {
    root: null,
    rootMargin: "0px",
    threshold
};

const handleIntersect = function(entries, observer) {
    entries.forEach(function(entry) {
        if (entry.intersectionRatio > threshold) {
            entry.target.classList.remove("reveal");
            entry.target.classList.remove("revealleft");
            entry.target.classList.remove("revealright");
            observer.unobserve(entry.target);
        }
    });
};

document.documentElement.classList.add("reveal-loaded");

// *** fix IE *** //

// Polyfill pour forEach pour IE 8 à 11
if (window.NodeList && !NodeList.prototype.forEach) {
    NodeList.prototype.forEach = function(callback, thisArg) {
        thisArg = thisArg || window;
        for (var i = 0; i < this.length; i++) {
            callback.call(thisArg, this[i], i, this);
        }
    };
}

const observer = new IntersectionObserver(handleIntersect, options);
const targets = document.querySelectorAll(".reveal, .revealleft, .revealright");
targets.forEach(function(target) {
    observer.observe(target);
});

// ********* Décalage du corps du site sous le menu général (qui est fixe) *********

$(window).on("resize", resize());
resize();

function resize() {
    if (sq.matches) {
        var headerHeight = $(".header--anchors").height();
    } else if (lq.matches) {
        var headerHeight =
            $(".header").height() + $(".header--anchors").height();
    } else {
        var headerHeight = $(".header").height();
    }

    $(".front--intro")[0].style.marginTop = headerHeight + "px";
}

$(".header--anchors__link:first-of-type").addClass(
    "header--anchors__link--active"
);
// ************ Click des ancres go to section ********
$(".js--go-to-section").click(function(e) {
    e.preventDefault();
    var targetname = "#" + $(this).data("target");
    var target = $(targetname);
    if (sq.matches) {
        var headerHeight = $(".header--anchors").height();
    } else if (lq.matches) {
        var headerHeight =
            $(".header").height() + $(".header--anchors").height();
    } else {
        var headerHeight = $(".header").height();
    }
    if (target.length) {
        $("html, body")
            .stop()
            .animate(
                { scrollTop: target.offset().top - headerHeight },
                800,
                "swing"
            );
    }
});

// ************ Ajout de la classe  Active sur l'ancre de la section active ********

var currentDay = -1;
var currentHour = 0;
var currentMinute = 0;

$(window)
    .scroll(function() {
        var scrollDistance = $(window).scrollTop() - 450;
        // Assign active class to nav links while scolling
        $(".js--section").each(function(i) {
            if ($(this).position().top <= scrollDistance) {
                $(".header--anchors__link")
                    .eq(i)
                    .addClass("header--anchors__link--active");
                $(".header--anchors__link")
                    .eq(i - 1)
                    .removeClass("header--anchors__link--active");
            } else if ($(this).position().top > scrollDistance) {
                $(".header--anchors__link")
                    .eq(i)
                    .removeClass("header--anchors__link--active");
            }
        });
    })
    .scroll();

// ************ Apparition du formulaire de la section intro au clic ********
$(".intro__contact--form").on("click", function() {
    $(".front--intro .hidden").removeClass("hidden");
});

$(".front--intro__form-title").on("click", function() {
    $(".front--intro .hidden").removeClass("hidden");
});

// ************ Apparition description membre de la team au click ********

$(".js--team-show").on("click", function() {
    var target = $(this).data("id");
    $(".front__team--card__description[data-id='" + target + "']").toggleClass(
        "front__team--card__description--hidden"
    );
});

// ********** Ajout d'un compte à rebours pour les promos ******* //
if ($(".front__prices--bloc front__prices--bloc__promo")) {
    var x = setInterval(function() {
        var date = new Date();
        var d = date.getDay();
        var h = date.getHours();
        var m = date.getMinutes();
        var s = date.getSeconds();

        if (d !== currentDay) {
            $(".js--places").html(7 - d);
            currentDay = d;
            if (d === 0) {
                var countDays = "00";
            } else {
                var countDays = "0" + (7 - d);
            }
            $(".js--days").html(countDays);
        }

        if (h !== currentHour) {
            currentHour = h;
            var countHours = format(23 - h);
            $(".js--hours").html(countHours);
        }

        if (m !== currentMinute) {
            currentMinute = m;
            var countMinutes = format(59 - m);
            $(".js--minutes").html(countMinutes);
        }

        var countSeconds = format(60 - s);
        $(".js--seconds").html(countSeconds);
    }, 1000);
}

function format(number) {
    if (number < 10) {
        return "0" + number;
    }
    return number;
}

// gestion des question FAQ
$(".front__faq--item").on("click", ".js--toggle-faq", function() {
    let $this = $(this);
    const item = $this.data("item");
    $this.toggleClass("front__faq--item__title__active");
    $(".front__faq--item__text[data-item='" + item + "']").toggleClass(
        "front__faq--item__text__active"
    );
});

$(".js--toggle-flags").on("click", function() {
    $(this).toggleClass("select-flags__active");
    $(".header--flags").toggleClass("header--flags__active");
});
